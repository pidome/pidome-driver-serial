/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.hardware.driver.serial;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fazecast.jSerialComm.SerialPort;
import org.pidome.platform.presentation.PresentationEnum;

/**
 * The serial port databit options.
 *
 * @author John Sirach
 */
public enum SerialPortStopBits implements PresentationEnum<Integer> {

    /**
     * 5 data bits.
     */
    STOPBITS_1("1", SerialPort.ONE_STOP_BIT),
    /**
     * 6 data bits.
     */
    STOPBITS_2("2", SerialPort.TWO_STOP_BITS),
    /**
     * 7 data bits.
     */
    STOPBITS_1_5("1.5", SerialPort.ONE_POINT_FIVE_STOP_BITS);

    /**
     * The description of this enum.
     */
    private final String label;

    /**
     * The stop bits asosciated with this enum.
     */
    private final int value;

    /**
     * Private enum constructor.
     *
     * @param enumLabel Set the enum description.
     * @param b Set the enum bits.
     */
    @JsonCreator
    SerialPortStopBits(@JsonProperty("label") final String enumLabel, @JsonProperty("value") final int b) {
        this.label = enumLabel;
        this.value = b;
    }

    /**
     * The description of the enum.
     *
     * @return The description.
     */
    public final String getLabel() {
        return this.label;
    }

    /**
     * The speed of the enum.
     *
     * @return The speed asociated with the enum.
     */
    @Override
    public final Integer getValue() {
        return this.value;
    }
}
