/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.hardware.driver.serial;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fazecast.jSerialComm.SerialPort;
import org.pidome.platform.presentation.PresentationEnum;

/**
 * The serial port parity support.
 *
 * @author John Sirach
 */
public enum SerialPortParity implements PresentationEnum<Integer> {

    /**
     * No parity.
     */
    PARITY_NONE("None", SerialPort.NO_PARITY),
    /**
     * Odd parity.
     */
    PARITY_ODD("Odd", SerialPort.ODD_PARITY),
    /**
     * Even parity.
     */
    PARITY_EVEN("Even", SerialPort.EVEN_PARITY),
    /**
     * Mark parity.
     */
    PARITY_MARK("Mark", SerialPort.MARK_PARITY),
    /**
     * Space parity.
     */
    PARITY_SPACE("Space", SerialPort.SPACE_PARITY);

    /**
     * The description of this enum.
     */
    private final String label;

    /**
     * The stop bits asosciated with this enum.
     */
    private final int value;

    /**
     * Private enum constructor.
     *
     * @param enumLabel Set the enum description.
     * @param p Set the enum parity.
     */
    @JsonCreator
    SerialPortParity(@JsonProperty("label") final String enumLabel, @JsonProperty("value") final int p) {
        this.label = enumLabel;
        this.value = p;
    }

    /**
     * The description of the enum.
     *
     * @return The description.
     */
    public final String getLabel() {
        return this.label;
    }

    /**
     * The speed of the enum.
     *
     * @return The speed asociated with the enum.
     */
    public final Integer getValue() {
        return this.value;
    }

}
