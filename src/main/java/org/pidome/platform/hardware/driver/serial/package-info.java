/**
 * Package providing the serial communication driver..
 * <p>
 * The serial driver.
 *
 * @since 1.0
 * @author John Sirach
 */
package org.pidome.platform.hardware.driver.serial;
