/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.hardware.driver.serial;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.pidome.platform.presentation.PresentationEnum;

/**
 * The serial port databit options.
 *
 * @author John Sirach
 */
public enum SerialPortDataBits implements PresentationEnum<Integer> {

    /**
     * 5 data bits.
     */
    DATABITS_5("5", 5),
    /**
     * 6 data bits.
     */
    DATABITS_6("6", 6),
    /**
     * 7 data bits.
     */
    DATABITS_7("7", 7),
    /**
     * 8 data bits.
     */
    DATABITS_8("8", 8);

    /**
     * The description of this enum.
     */
    private final String label;

    /**
     * The data bits asosciated with this enum.
     */
    private final int value;

    /**
     * Private enum constructor.
     *
     * @param enumLabel Set the enum label.
     * @param b Set the enum bits.
     */
    @JsonCreator
    SerialPortDataBits(@JsonProperty("label") final String enumLabel, @JsonProperty("value") final int b) {
        this.label = enumLabel;
        this.value = b;
    }

    /**
     * The description of the enum.
     *
     * @return The description.
     */
    public final String getLabel() {
        return this.label;
    }

    /**
     * The speed of the enum.
     *
     * @return The speed asociated with the enum.
     */
    public final Integer getValue() {
        return this.value;
    }
}
