/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.hardware.driver.serial;

import io.vertx.core.Future;
import java.util.Arrays;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.pidome.platform.hardware.driver.HardwareDriver;
import org.pidome.platform.hardware.driver.HardwareDriverDiscovery;
import org.pidome.platform.hardware.driver.config.SerialDeviceConfiguration;
import org.pidome.platform.hardware.driver.interfaces.SerialDriverInterface;
import org.pidome.platform.presentation.PresentationSection;
import org.pidome.platform.presentation.input.fields.DisplayInput;
import org.pidome.platform.presentation.input.fields.SelectEnumInput;

/**
 * The PiDome serial driver for serial communications over USB or COM ports.
 *
 * @author John Sirach
 */
@HardwareDriverDiscovery(
        name = "PiDome Serial driver",
        description = "Driver natively provided by PiDome."
)
public class PiDomeSerialDriver extends HardwareDriver implements SerialDriverInterface {

    /**
     * Get logger.
     */
    private static final Logger LOG = LogManager.getLogger(PiDomeSerialDriver.class);

    /**
     * Constructor.
     */
    public PiDomeSerialDriver() {

    }

    /**
     * Provides the configuration of an attached peripheral.
     *
     * @param configuration The configuration of an attached peripheral.
     * @param future Used to determine when the configuration is done by the
     * server.
     */
    @Override
    public void configure(SerialDeviceConfiguration configuration, Future<Void> future) {

        PresentationSection infoSection = new PresentationSection("Device information");
        DisplayInput deviceName = new DisplayInput("deviceName", "Device Name");
        deviceName.setContent(configuration.getName());

        DisplayInput devicePort = new DisplayInput("devicePort", "Device port");
        devicePort.setContent(configuration.getPort());

        infoSection.setElements(deviceName, devicePort);

        PresentationSection section = new PresentationSection("Set the serial options");

        SelectEnumInput<SerialPortSpeed> portSpeed = new SelectEnumInput<>("portspeed", "Port Speed", "Set the port speed");
        portSpeed.setListValues(Arrays.asList(SerialPortSpeed.values()));
        portSpeed.setDefaultValue(SerialPortSpeed.BAUDRATE_9600);

        SelectEnumInput<SerialPortDataBits> dataBits = new SelectEnumInput<>("databits", "Set Data bits", "Set the data bits");
        dataBits.setListValues(Arrays.asList(SerialPortDataBits.values()));
        dataBits.setDefaultValue(SerialPortDataBits.DATABITS_8);

        SelectEnumInput<SerialPortParity> parity = new SelectEnumInput<>("stopbits", "Set stop bits", "Set the stop bits");
        parity.setListValues(Arrays.asList(SerialPortParity.values()));
        parity.setDefaultValue(SerialPortParity.PARITY_NONE);

        SelectEnumInput<SerialPortStopBits> stopBits = new SelectEnumInput<>("stopbits", "Set stop bits", "Set the stop bits");
        stopBits.setListValues(Arrays.asList(SerialPortStopBits.values()));
        stopBits.setDefaultValue(SerialPortStopBits.STOPBITS_1);

        section.setElements(Arrays.asList(portSpeed, dataBits, parity, stopBits));

        this.getConfiguration().addSections(infoSection, section);

        future.complete();

    }

    /**
     * Sets the options for the serial driver.
     *
     * @param future The future to handle the completion or failure.
     */
    @Override
    public final void setOpts(final Future<Void> future) {
        future.succeeded();
        LOG.debug("Options set");
    }

    /**
     * Starts the serial implementation.
     *
     * @param future The future to complete or fail.
     */
    @Override
    public final void startDriver(final Future<Void> future) {
        future.succeeded();
        LOG.debug("Driver started");
    }

    /**
     * Stops the serial implementation.
     *
     * @param future The future to ack the driver is stopped.
     */
    @Override
    public final void stopDriver(final Future<Void> future) {
        future.succeeded();
        LOG.debug("Driver stopped");
    }

}
