/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.pidome.platform.hardware.driver.serial;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.pidome.platform.presentation.PresentationEnum;

/**
 * Collection of supported serial port speeds.
 *
 * @author John Sirach
 */
public enum SerialPortSpeed implements PresentationEnum<Integer> {

    /**
     * 300 Baud.
     */
    BAUDRATE_300("300 baud", 300),
    /**
     * 600 Baud.
     */
    BAUDRATE_600("600 baud", 600),
    /**
     * 1200 Baud.
     */
    BAUDRATE_1200("1200 baud", 1200),
    /**
     * 4800 Baud.
     */
    BAUDRATE_4800("4800 baud", 4800),
    /**
     * 9600 Baud.
     */
    BAUDRATE_9600("9600 baud", 9600),
    /**
     * 14400 Baud.
     */
    BAUDRATE_14400("1440 baud", 14400),
    /**
     * 19200 Baud.
     */
    BAUDRATE_19200("19200 baud", 19200),
    /**
     * 38400 Baud.
     */
    BAUDRATE_38400("38400 baud", 38400),
    /**
     * 57600 Baud.
     */
    BAUDRATE_57600("57600 baud", 57600),
    /**
     * 115200 Baud.
     */
    BAUDRATE_115200("115200 baud", 115200);

    /**
     * The description of this enum.
     */
    private final String label;

    /**
     * The speed asosciated with this enum.
     */
    private final int value;

    /**
     * Private enum constructor.
     *
     * @param enumLabel Set the enum description.
     * @param s Set the enum speed.
     */
    @JsonCreator
    SerialPortSpeed(@JsonProperty("label") final String enumLabel, @JsonProperty("value") final int s) {
        this.label = enumLabel;
        this.value = s;
    }

    /**
     * The description of the enum.
     *
     * @return The description.
     */
    public final String getLabel() {
        return this.label;
    }

    /**
     * The speed of the enum.
     *
     * @return The speed asociated with the enum.
     */
    public final Integer getValue() {
        return this.value;
    }

}
