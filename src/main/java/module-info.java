/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

module org.pidome.platform.hardware.driver.serial {
    opens org.pidome.platform.hardware.driver.serial;
    requires vertx.core;
    requires org.pidome.platform.hardware.driver;
    requires org.pidome.platform.presentation;
    requires org.apache.logging.log4j;
    requires com.fazecast.jSerialComm;
}
